"""
City Finder Processor
---------------------

This is an example of a Lintol processor. You can run it like so:

    python3 processor.py out-example-2021-02-01-hansard-plenary.txt

or, if you would like a nicely-formatted HTML page to look at:

    ltldoorstep -o html --output-file output.html process sample_transcripts/out-example-2021-02-01-hansard-plenary.txt
    processor.py -e dask.threaded

This will create output.html in the current directory and, in a browser (tested with Chrome),
should look like output.png.
"""

import re
import sys
import logging
from dask.threaded import get

from ltldoorstep.processor import DoorstepProcessor
from ltldoorstep.aspect import AnnotatedTextAspect
from ltldoorstep.reports.report import combine_reports
from ltldoorstep.document_utils import load_text, split_into_paragraphs


import re
import sys
import logging
from dask.threaded import get

from ltldoorstep.processor import DoorstepProcessor
from ltldoorstep.aspect import AnnotatedTextAspect
from ltldoorstep.reports.report import combine_reports
from ltldoorstep.document_utils import load_text, split_into_paragraphs


CONSTS = [
    'antrim east', 'east antrim', 'antrim north', 'north antrim', 'antrim south', 'south antrim', 'upper bann',
    'belfast east', 'east belfast', 'belfast north', 'north belfast', 'belfast south', 'south belfast', 'belfast west',
    'west belfast', 'down north', 'north down', 'down south', 'south down', 'fermanagh and south tyrone',
    'south tyrone', 'fermanagh', 'foyle', 'lagan valley', 'londonderry east', 'east londonderry', 'newry and armagh',
    'newry', 'armagh', 'strangford', 'west tyrone', 'tyrone west', 'mid ulster'
]


def const_finder(text, rprt):
    """
    Add report items to indicate where cities appear, and how often in total
    """

    # This doorstep utility splits a big text into paragraphs, and standardizes some of
    # the punctuation. We do this splitting so that it is easier to view the results, rather
    # than scrolling through highlighted lines in one big document.
    paragraphs = split_into_paragraphs(text)

    # This is our counter for cities - we initialize every city's count to 0
    const_counts = {const: 0 for const in CONSTS}

    # Now we loop through the paragraphs - `enumerate` gives us a running count in `para_n`
    for para_n, (paragraph, line_number) in enumerate(paragraphs):

        # As mentioned above, we will search entirely in lowercase - a quick way of doing
        # a case insensitive search (others welcome too!)
        paragraph_lower = paragraph.lower()

        # We check this paragraph for city names...
        for const in CONSTS:
            # This gets a total count for the city
            const_occurrences = paragraph_lower.count(const)

            # If it's not 0, we will need to add an item to our report
            if const_occurrences > 0:
                # ...but first, update our overall count for this city
                # (we will need this down below)
                const_counts[const] += const_occurrences

                # To let us highlight words or phrases, we use an "Aspect"
                # This wraps a phrase/snippet/paragraph, and we add
                # one or more notes to highlight words or phrases within it.
                content = AnnotatedTextAspect(paragraph)

                # This loop uses Regular Expressions to search through
                # all the occurrences of the city's (lowercase) name in the
                # lowercase paragraph. Note that regular expressions have
                # their own language for doing more complex searches - because
                # the `city` variable only contains letters and numbers, this
                # works easily, but you might need to be careful if searching for something
                # with punctuation as well.
                for match in re.finditer(const, paragraph_lower):
                    # We found an occurrence of the city, now we add it to the report!
                    # The necessary arguments are:
                    #    note  - the comment that should pop up
                    #            if you put your mouse over the highlighted text
                    #    start_offset and end_offset
                    #          - where the highlighting should start and end _relative
                    #            to the paragraph_ in characters.
                    #    level - which urgency group you want to put it in
                    #            (logging.INFO, logging.WARNING, logging.ERROR)
                    #            it's up to you.
                    #    tags  - (optional) any additional tags you want to make up.


                    content.add(
                        note=f'Occurence of {const.title()} cropped up',
                        start_offset=match.start(),
                        end_offset=match.end(),
                        level=logging.INFO,
                        tags=['const']
                    )

                # Maybe NI's capital gets too much attention? Maybe not?
                # This will emphasize uses of Belfast by putting them into a
                # warning group at the top.
                if const == 'north antrim':
                    urgency = logging.WARNING
                else:
                    urgency = logging.INFO


                # Finally, we add our summary for this city in this paragraph to
                # the overall report.
                x=8
                rprt.add_issue(
                    urgency,
                    f'const-cropped-up hi does this work',

                    f'Found {const}\n\n hiii{x}',
                    line_number=line_number,
                    character_number=0,
                    content=content
                )

    # Not all things we want to report have a location in the document - that's OK.
    # Here, we go through and a report item to display the total count for each city
    # in the entire document.
    for const, total in const_counts.items():
        rprt.add_issue(
            logging.INFO,
            'city-totals',
            f'Found {total} occurrences of {const.title()}'
        )

    return rprt


# find a way to add in '
NAMES = [
    "mr speaker:", 'mr allister:', 'mrs foster:', "mr otoole:"
    'mrs oneill:', 'mr nesbitt:', 'mr blair:', 'mr deputy speaker (mr mcglone):',
    'mr mccrossan (the chairperson of the audit committee):', 'mr carroll:', 'mr mccrossan:',
    'mrs d kelly:', 'mr odowd:', 'dr aiken:', 'mr deputy speaker (mr beggs):',
    'mr murphy (the minister of finance):', 'mr murphy:', 'mr otoole:', 'mr murphy:',
    'dr aiken (the chairperson of the committee for finance):', 'mr frew:', 'mr mccrossan:', 'a member:', 'mr lyttle:',
    'mr acaleer (the chairperson of the committee for agriculture, environment and rural affairs):',
    'mr mcgrath (the chairperson of the committee for the executive office):',
    'mr mcgrath:', 'mr lyttle (the chairperson of the committee for education):',
    'miss mcilveen (the chairperson of the committee for infrastructure):',
    'mr gildernew (the chairperson of the committee for health)',
    'mr lyons (the minister of agriculture, environment and rural affairs):', 'mr g kelly:', 'mr lyons:',
    'mr humphrey:', 'mr buckley:',
]


def name_finder(text, rprt):
    """
    Add to NAMES to indicate when each MLA has spoken and how often
    """

    names = NAMES.sort()

    # Doorstep utility to split big text into paragraphs and standardise punctuation
    paragraphs = split_into_paragraphs(text)

    # This is our counter for cities - we initialize every name's count to 0
    name_counts = {name: 0 for name in NAMES}

    # We check this paragraph for mla names...
    for name in NAMES:
        # Now we loop through the paragraphs - `enumerate` gives us a running count in `para_n`
        for para_n, (paragraph, line_number) in enumerate(paragraphs):

            # As mentioned above, we will search entirely in lowercase
            paragraph_lower = paragraph.lower()

            # This gets a total count for the mla
            name_occurrences = paragraph_lower.count(name)

            # If it's not 0, we will need to add an item to our report
            if name_occurrences > 0:
                # ...but first, update our overall count for this mla
                # (we will need this down below)
                name_counts[name] += name_occurrences

                # To let us highlight words or phrases, we use an "Aspect"
                # This wraps a phrase/snippet/paragraph, and we add
                # one or more notes to highlight words or phrases within it.
                content = AnnotatedTextAspect(paragraph)

                # This loop uses Regular Expressions to search through
                # all the occurrences of the city's (lowercase) name in the
                # lowercase paragraph
                for match in re.finditer(name, paragraph_lower):
                    # We found an occurrence of the name, now we add it to the report!
                    # The necessary arguments are:
                    #    note  - the comment that should pop up
                    #            if you put your mouse over the highlighted text
                    #    start_offset and end_offset
                    #          - where the highlighting should start and end _relative
                    #            to the paragraph_ in characters.
                    #    level - which urgency group you want to put it in
                    #            (logging.INFO, logging.WARNING, logging.ERROR)
                    #            it's up to you.
                    #    tags  - (optional) any additional tags you want to make up.
                    content.add(
                        note=f'Occurence of {name.title()}',
                        start_offset=match.start(),
                        end_offset=match.end(),
                        level=logging.INFO,
                        tags=['name']
                    )

                # The first minister's comments are of particular interest
                if name == 'mrs foster:':
                    urgency = logging.WARNING
                else:
                    urgency = logging.INFO

                # Finally, we add our summary for this mla in this paragraph to
                # the overall report.
                rprt.add_issue(
                    urgency,
                    'name-cropped-up',
                    f'Spoken by {name.title()}',
                    line_number=line_number,
                    character_number=0,
                    content=content
                )

    # Not all things we want to report have a location in the document - that's OK.
    # Here, we go through and a report item to display the total count for each city
    # in the entire document.
    for name, total in name_counts.items():
        rprt.add_issue(
            logging.INFO,
            'name-totals',
            f'Found {total} occurrences of {name.title()}'
        )

    return rprt


class MLAFinderProcessor(DoorstepProcessor):
    """
    This class wraps some of the Lintol magic under the hood, that lets us plug
    our city finder into the online version, and create reports mixing and matching
    from various processors.
    """

    # This is the type of report we create - it could be tabular (e.g. CSV), geospatial
    # (e.g. GeoJSON) or document, as in this case.
    preset = 'document'

    # This is a unique code and version to identity the processor. The code should be
    # hyphenated, lowercase, and start with lintol-code-challenge
    code = 'lintol-code-challenge-mla-name-finder:1'

    # This is a short phrase or description explaining the processor.
    description = "MLA Name Finder for Lintol Coding Challenge"

    # Some of our processors get very complex, so this lets us build up execution graphs
    # However, for the coding challenge, you probably only want one or more steps.
    # To add two more, create functions like city_finder called town_finder and country_finder,
    # then uncomment the code in this function (and remove the extra parenthesis in the 'output' line)
    def get_workflow(self, filename, metadata={}):
        workflow = {
            'load-text': (load_text, filename),
            'get-report': (self.make_report,),
            # we only want to run the name finder in our code
            'step-A': (name_finder, 'load-text', 'get-report'),
            # 'step-B': (const_finder, 'load-text', 'get-report'),
            'output': (workflow_condense, 'step-A')
        }
        return workflow


# If there are several steps, this final function pulls them into one big report.
def workflow_condense(base, *args):
    return combine_reports(*args, base=base)


# This is the actual variable Lintol looks for to set up the processor - you
# shouldn't need to touch it (except to change the class name, if neeeded)
processor = MLAFinderProcessor.make

# Lintol will normally execute this processor in its own magical way, but you
# can also run it via the command line without using ltldoorstep at all (just the
# libraries already imported). The code below lets this happen, and prints out a
# JSON version of the report.
if __name__ == "__main__":
    argv = sys.argv
    processor = MLAFinderProcessor()
    processor.initialize()
    workflow = processor.build_workflow(argv[1])
    print(get(workflow, 'output'))
