# MLA Contribution Processor for NI Assembly Hansard Minutes
Most of the public can name at least one of their MLAs, but they seldom know what goes on behind the chambers of government. Our project aims to let users see what exactly their MLA has said and how many times they have spoken in the devolved parliament by providing the Hansard transcripts in an easy-to-read alphabetical order of speaker. This can let you jump straight to your MLA's heartwrenching speech, or get some juicy quotes for the media. 

## Using the Repository
1. In the main repository folder, install the requirements:
```
pip install -r requirements.txt
```

2. Provide your own XML file with Hansard Data and create a filtered txt file using the following commands. Ensure you replace ```data0201.xml``` with your own XML file:
```
cd sample_transcripts
python hansard_prepper_mj.py data0201.xml
```

3. Once complete, generate the HTML file with the following command:
```
ltldoorstep -o html --output-file output.html process sample_transcripts/out-data0201.txt processor.py -e dask.threaded
```

4. To create a JSON file, run the following command:
```
ltldoorstep -o json --output-file output.json process sample_transcripts/out-data0201.txt processor.py -e dask.threaded
```

## Examples
Below is the output from the hansard_prepper_mj.py file when using XML data

![output text file example](pics/example_txt_file.png "Text File")

Below is an example HTML output for each speaker in aphabetical order. Note you can see where they speak in the text file so you can go snooping

![output HTML file example](pics/example_html_file.png "HTML File")

## Credit
This project was adapted from the [Lintol Hansard Processor](https://gitlab.com/lintol/coding-challenge-2021-exemplar-1), an open source software initiative to cleanse raw data.

## Authors
We are a team of students, studying Electronic and Mechatronic Engineering in Ulster University:
- Emma Jamison
- Luke Campbell
- Michael Jennings
