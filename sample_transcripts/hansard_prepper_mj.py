import sys
import xml.etree.ElementTree as ET


class HansardTextExtractor:
    """
    Extract text from Hansard data into paragraphs by each speaker, separated by a ----
    """
    def run(self, xml_filename):
        """
        :param xml_filename: the file to be parsed e.g. data0201.xml
        """
        etree = ET.parse(xml_filename)

        proceedings_plaintext = ''  # this will be the text output

        for component in etree.getroot():
            if component.tag == 'HansardComponent':     # filter by each HansardComponent in the XML
                component_type_node = component.find('ComponentType')   # strip out the type e.g. Speaker (MlaName)
                component_text_node = component.find('ComponentText')   # strip out the text e.g. Mr Dodds
                if component_text_node.text and component_type_node.text:   # check both exist
                    type = component_type_node.text.strip()     # get the type as plain text
                    if 'Speaker' in type:                       # if it is a speaker then take a new section
                        speaker = component_text_node.text.strip()  # get the speaker name
                        proceedings_plaintext += f'\n\n----\n\n{speaker} '  # add the speaker to the output
                    elif component_type_node.text.strip() == 'Spoken Text':     # get the spoken text
                        text = component_text_node.text.replace('\n', '')
                        text = text.replace('<BR />', ' ')
                        proceedings_plaintext += f'{text} '
                    elif component_type_node.text.strip() == 'Header':  # headers will be included by broken-up
                        proceedings_plaintext += f'\n\n----\n\n[{component_text_node.text.upper()}]'

        text_filename = xml_filename.replace('.xml', '.txt')    # the output should be a txt file
        # multiple utf-8 errors were introduced before. Make sure they are converted using encode/decode
        proceedings_plaintext_unicode = proceedings_plaintext.encode("utf-8", 'replace').decode('utf-8')
        with open(text_filename, 'w', encoding='utf-8') as text_file:
            text_file.write(proceedings_plaintext_unicode)


if __name__ == "__main__":
    try:
        filename = sys.argv[1]  # this is the xml file to be precessed e.g. data0201.xml
    except KeyError as e:
        print("Hansard Cleaner takes one argument, the XML file to be cleaned.")
        raise e

    extractor = HansardTextExtractor()
    extractor.run(filename)
