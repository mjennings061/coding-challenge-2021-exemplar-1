import os


# os.system(
#     "ltldoorstep -o html --output-file output.html process " +
#     "sample_transcripts/data0201.xml processor.py -e dask.threaded"
# )

# os.system(
#     "ltldoorstep -o html --output-file output.html process " +
#     "sample_transcripts/out-example-2021-02-01-hansard-plenary.txt processor.py -e dask.threaded"
# )

os.system(
    "ltldoorstep -o json --output-file output.json process " +
    "sample_transcripts/out-data0201.txt processor.py -e dask.threaded"
)

os.system(
    "ltldoorstep -o html --output-file output.html process " +
    "sample_transcripts/out-data0201.txt processor.py -e dask.threaded"
)
